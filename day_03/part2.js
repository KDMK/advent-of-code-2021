const fs = require("fs");

const args = process.argv.slice(2);
const inputNumbers = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(v => {
        if (v.trim() === '') {
            return [];
        }

        return v.split("").map(v => Number.parseInt(v));
    });


function binaryToDecimal(array) {
    let result = 0;
    for (let i = 0; i < array.length; i++) {
        result += (Math.pow(2, i) * array[array.length - 1 - i]);
    }

    return result;
}

function createBuffer(sourceArray) {
    const buffer = [];

    for (let i = 0; i < sourceArray[0].length; i++) {
        buffer.push({ ones: 0, zeroes: 0 });
    }

    return buffer;
}

function findOnesAndZeroes(input) {
    const buffer = createBuffer(input);

    for (let number of input) {
        for (let i = 0; i < number.length; i++) {
            if (number[i] === 0) {
                buffer[i].zeroes++;
            } else {
                buffer[i].ones++;
            }
        }
    }

    return buffer;
}

function findRateFactors(inputArray) {
    const filtered = findOnesAndZeroes(inputArray)

    const oxyGen = filtered.map(v => {
        if (v.zeroes > v.ones) {
            return 0;
        } else {
            return 1;
        }
    });
    
    const co2Gen = filtered.map(v => {
        if (v.zeroes > v.ones) {
            return 1;
        } else {
            return 0;
        }
    });

    return {
        oxyGen,
        co2Gen
    }
}

let i = 0;
let result1 = Object.assign([], inputNumbers);

while(result1.length > 1) {
    const rateFactors = findRateFactors(result1);

    result1 = result1.flatMap(v => {
        if(rateFactors.oxyGen[i] == v[i]) {
            return [v]
        }

        return []
    });

    i++;
}

i = 0;
let result2 = Object.assign([], inputNumbers);

while(result2.length > 1) {
    const rateFactors = findRateFactors(result2);

    result2 = result2.flatMap(v => {
        if(rateFactors.co2Gen[i] == v[i]) {
            return [v]
        }

        return []
    });

    i++;
}

const out1 = binaryToDecimal(result1[0]);
const out2 = binaryToDecimal(result2[0]);

console.log(out1 * out2);