const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(v => {
        if (v.trim() === '') {
            return [];
        }

        return v.split("").map(v => Number.parseInt(v));
    });


function binaryToDecimal(array) {
    let result = 0;
    for (let i = 0; i < array.length; i++) {
        result += (Math.pow(2, i) * array[array.length - 1 - i]);
    }

    return result;
}

const buffer = [];
for (let i = 0; i < input[0].length; i++) {
    buffer.push({ ones: 0, zeroes: 0 });
}


for (let number of input) {
    for (let i = 0; i < number.length; i++) {
        if (number[i] === 0) {
            buffer[i].zeroes++;
        } else {
            buffer[i].ones++;
        }
    }
}

const result1 = buffer.map(v => {
    if (v.zeroes > v.ones) {
        return 0;
    } else {
        return 1;
    }
});

const result2 = buffer.map(v => {
    if (v.zeroes > v.ones) {
        return 1;
    } else {
        return 0;
    }
});

const binaryRes1 = binaryToDecimal(result1);
const binaryRes2 = binaryToDecimal(result2);

console.log(binaryRes1 * binaryRes2);