const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")[0]
    .split(",")
    .map(v => Number.parseInt(v));

function median(array) {
    let sorted = array.sort((a, b) => {
        if (a > b) return 1;
        else if (a < b) return -1;
        else return 0;
    });

    return sorted.length % 2 === 0 ? sorted[sorted.length / 2] : 
      (sorted[(sorted.length - 1)/2] + sorted[(sorted.length + 1) / 2]) / 2;
}

function fuelCost(array) {
    let medianValue = median(array);
    let totalCost = 0;
   
    for(let value of array) {
        totalCost += Math.abs(medianValue - value);
    }
    
    return totalCost;
}

console.log(fuelCost(input))