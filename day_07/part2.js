const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")[0]
    .split(",")
    .map(v => Number.parseInt(v));

// for the sake of this task, rounded average always exist in this input
function average(array) {
    let sum = array.reduce((agg, cur) => agg + cur, 0);
    return Math.round(sum / array.length);
}

function median(array) {
    let sorted = array.sort(intComparator);

    return sorted.length % 2 === 0 ? sorted[sorted.length / 2] :
        (sorted[(sorted.length - 1) / 2] + sorted[(sorted.length + 1) / 2]) / 2;
}

function intComparator(a, b) {
    if (a > b)
        return 1;
    else if (a < b)
        return -1;
    else
        return 0;
}

// with step 1
function seriesSum(a1, an) {
    return (1 + Math.abs(a1 - an)) * (a1 + an) / 2;
}

function fuelCost(array) {
    let avg = average(array) - 1; // not quite sure why -1

    let totalCost = 0;
    for (let value of array) {
        totalCost += seriesSum(1, Math.abs(avg - value));
    }

    return totalCost;
}

console.log(fuelCost(input));