const fs = require("fs");

const counts = Array(9).fill(0);

const args = process.argv.slice(2);
fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")[0]
    .split(",")
    .forEach(v => {
        const age = Number.parseInt(v)
        counts[age] += 1;
    });

const DAYS = 256;

console.log(counts);

for (let i = 0; i < DAYS; i++) {
    const newCount = counts.shift();
    counts[6] += newCount;
    counts.push(newCount);
}

console.log('total:', counts.reduce((count, total) => count + total));
