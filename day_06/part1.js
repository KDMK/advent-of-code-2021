const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")[0]
    .split(",")
    .map(v => {
        const age = Number.parseInt(v)
        return {age};
    });

const DAYS = 80;

for(let i = 0; i < DAYS; i++) {
    const newFish = [];
    for(const fish of input) {
        if(fish.age <= 0) {
            newFish.push({age: 8});
            fish.age = 7;
        }

        fish.age--;
    }
    input.push(...newFish);
}

console.log(input.length);