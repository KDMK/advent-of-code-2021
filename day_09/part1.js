const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(line => {
        return line.split("").map(c => Number.parseInt(c));
    });

function findLocalMinimums(matrix) {
    const minimums = [];

    const rows = matrix.length;
    const cols = matrix[0].length;

    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            if (j > 0 && matrix[i][j] >= matrix[i][j - 1]) {
                continue;
            }

            if (j < cols - 1 && matrix[i][j] >= matrix[i][j + 1]) {
                continue;
            }

            if (i > 0 && matrix[i][j] >= matrix[i - 1][j]) {
                continue;
            }

            if (i < rows - 1 && matrix[i][j] >= matrix[i + 1][j]) {
                continue;
            }

            minimums.push(matrix[i][j])
        }
    }

    return minimums;
}

function calculateRiskFactor(minimums) {
    return minimums.reduce((agg,cur) => 1+agg+cur, 0);
}

const minimums = findLocalMinimums(input);

console.log(calculateRiskFactor(minimums));