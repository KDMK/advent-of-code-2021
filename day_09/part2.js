const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .flatMap(line => {
        if (line.trim() === "") return [];
        return [line.split("").map(c => Number.parseInt(c))];
    });

const coloringMatrix = [];
for (let i = 0; i < input.length; i++) {
    coloringMatrix.push(new Array(input[0].length).fill(0));
}

function printMatrix(matrix) {
    for (let row of matrix) {
        console.log(row.join(""))
    }
}

function findLocalMinimums(matrix) {
    const minimums = [];

    const rows = matrix.length;
    const cols = matrix[0].length;

    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            if (j > 0 && matrix[i][j] >= matrix[i][j - 1]) {
                continue;
            }

            if (j < cols - 1 && matrix[i][j] >= matrix[i][j + 1]) {
                continue;
            }

            if (i > 0 && matrix[i][j] >= matrix[i - 1][j]) {
                continue;
            }

            if (i < rows - 1 && matrix[i][j] >= matrix[i + 1][j]) {
                continue;
            }

            minimums.push({ row: i, col: j })
        }
    }

    return minimums;
}

function floodFill(x, y, matrix, coloringMatrix, containedPoints) {
    if (typeof matrix[x] === 'undefined') {
        return;
    }

    if (typeof matrix[x][y] === 'undefined') {
        return
    }

    if (matrix[x][y] == 9 ||
        coloringMatrix[x][y] == 1) {
        return;
    }

    if (matrix[x][y] >= 9) {
        return;
    }

    coloringMatrix[x][y] = 1;
    containedPoints.push({ row: x, col: y });

    floodFill(x + 1, y, matrix, coloringMatrix, containedPoints);  // then i can either go south
    floodFill(x - 1, y, matrix, coloringMatrix, containedPoints);  // or north
    floodFill(x, y + 1, matrix, coloringMatrix, containedPoints);  // or east
    floodFill(x, y - 1, matrix, coloringMatrix, containedPoints);  // or west
}

function intComparator(a, b) {
    if (a > b)
        return 1;
    else if (a < b)
        return -1;
    else
        return 0;
}

function floodSize(x, y, matrix, visited) {
    if (typeof matrix[x] === 'undefined') {
        return 0;
    }

    if (typeof matrix[x][y] === 'undefined') {
        return 0;
    }

    if (coloringMatrix[x][y] == 0) {
        return 0;
    }

    if (visited[x][y] == 1) {
        return 0;
    }

    visited[x][y] = 1;
    sum = 1;

    sum += floodSize(x + 1, y, matrix, visited);  // then i can either go south
    sum += floodSize(x - 1, y, matrix, visited);  // or north
    sum += floodSize(x, y + 1, matrix, visited);  // or east
    sum += floodSize(x, y - 1, matrix, visited);  // or west

    return sum;
}

function islandSizes(minimums, ponds) {
    let poolSizes = []

    for (let minimum of minimums) {
        const visitedMatrix = [];
        
        for (let i = 0; i < input.length; i++) {
            visitedMatrix.push(new Array(input[0].length).fill(0));
        }

        poolSizes.push(floodSize(minimum.row, minimum.col, ponds, visitedMatrix))
    }

    return poolSizes.sort(intComparator);
}

let minimums = findLocalMinimums(input);
let containedPoints = [];

for (let minimum of minimums) {
    floodFill(minimum.row, minimum.col, input, coloringMatrix, containedPoints);
}

let sizes = islandSizes(minimums, coloringMatrix);
sizes = sizes.slice(sizes.length - 3);

console.log(sizes.reduce((agg,cur) => agg*cur, 1));