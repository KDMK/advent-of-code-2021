const fs = require("fs");

/************** Functions definition *******************/
function checkBoard(matrix) {
    // check horizontal combinations
    for (let i = 0; i < matrix.length; i++) {
        valid = true;
        if (matrix[i][0]) {
            for (let j = 1; j < matrix.length; j++) {
                if (!matrix[i][j]) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                return true;
            }
        }
    }

    // check vertical combinations
    for (let i = 0; i < matrix.length; i++) {
        valid = true;
        if (matrix[0][i]) {
            for (let j = 1; j < matrix.length; j++) {
                if (!matrix[j][i]) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                return true;
            }
        }
    }

    return false;
}

function createMatrix(rows, cols) {
    const retVal = [];
    for (let i = 0; i < rows; i++) {
        retVal.push(new Array(cols).fill(false));
    }

    return retVal;
}

/************** Reading input & Preparing *************/
const args = process.argv.slice(2);
let input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(l => l.trim());

const drawnNumbers = input[0].split(',')
    .map(v => Number.parseInt(v));

input = input.slice(2);

const boards = [];
const boardsTransformed = [];
const boardsTag = [];

/************** Create boards from input **************************/
let buffer = [];
for (let line of input) {
    if (line.trim() == '') {
        boards.push(buffer);
        buffer = [];
        continue;
    }

    buffer.push(line.split(/\s+/).map(v => Number.parseInt(v.trim())));
}
boards.push(buffer);

/********************** Transform boards for easier lookup ************/
for (let board of boards) {
    const boardTransformed = {};

    for (let i = 0; i < board.length; i++) {
        for (let j = 0; j < board.length; j++) {
            boardTransformed[board[i][j]] = { x: i, y: j }
        }
    }

    boardsTransformed.push(boardTransformed);
}

/********************* Create boards for crossing *********************/
boards.forEach(() => {
    boardsTag.push(createMatrix(5, 5));
})

let lastNumber;
let winningBoardIndex;

for (let number of drawnNumbers) {
    for (let i = 0; i < boards.length; i++) {
        if (typeof boardsTransformed[i][number] != 'undefined') {
            let boardTag = boardsTag[i];
            let coordinates = boardsTransformed[i][number];

            boardTag[coordinates.x][coordinates.y] = true;
        }
    }

    let bingo = false;
    for (let i = 0; i < boardsTag.length; i++) {
        let board = boardsTag[i];

        if (checkBoard(board)) {
            lastNumber = number;
            winningBoardIndex = i;
            bingo = true;
            break;
        }
    }

    if (bingo) {
        break;
    }
}

let sum = 0;
let winningBoardTag = boardsTag[winningBoardIndex];
let winningBoard =  boards[winningBoardIndex];
for(let i = 0; i < winningBoardTag.length; i++) {
    for(let j = 0; j < winningBoardTag[0].length; j++) {
        if (!winningBoardTag[i][j]) {
            sum += winningBoard[i][j];
        }
    }
}

console.log(lastNumber * sum);