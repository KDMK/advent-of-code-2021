#!/bin/bash

nvm use 14.18.1

function print_instructions {
  echo "Usage: run 1 1"
  echo "This will run part one and compare it with expected output 1"
}

function run {
  if [ "$#" -eq 0 ]; then
    print_instructions
    return 0;
  fi;

  output=$(node "part$1.js" "input$2.txt")

  if [ -z "$output" ]; then
    echo "There is nothing for comparison!"
    return 1
  fi
 
  diff "part$1_output$2.txt" <(echo $output) > /dev/null

  if [ $? -eq 0 ]; then
    echo -e "\033[0;32mYay!\033[0m Correct answers is $output"
  else
    echo -e "\033[0;31mOh-o!\033[0m $output is not correct answers."
  fi	
}
