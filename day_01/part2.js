const fs = require("fs");

const args = process.argv.slice(2);
const numbers = fs.readFileSync(`./${args[0]}`, "utf-8")
	.split("\n")
	.flatMap(v => {
	  if(v.trim() === '') {
	    return [];
          }
	  return [Number.parseInt(v)];
	});

let higher = 0;
let lastSum = numbers[2] + numbers[1] + numbers[0];
for(let i = 3; i < numbers.length; ++i) {
  let curSum = numbers[i] + numbers[i-1] + numbers[i-2];
  if (curSum > lastSum) {
    higher++;
  }
  lastSum = curSum;
}

console.log(higher);
