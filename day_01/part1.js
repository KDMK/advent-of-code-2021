const fs = require("fs");

const args = process.argv.slice(2);
const numbers = fs.readFileSync(`./${args[0]}`, "utf-8")
	.split("\n")
	.flatMap(v => {
	  if(v.trim() === '') {
	    return [];
          }
	  return [Number.parseInt(v)];
	});

let higher = 0;
for(let i = 1; i < numbers.length; i++) {
  if(numbers[i-1] < numbers[i]) {
    ++higher;
  }
}

console.log(higher);
