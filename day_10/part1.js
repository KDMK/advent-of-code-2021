const fs = require("fs");

const openingSegments = new Set(['(', '{', '[', '<']);
const closingSegments = new Set([")", "}", "]", ">"]);

const pairs = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">",
}

const peek = (stack) => stack[stack.length - 1]

let scoringTable = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(line => line.split(""));

const wrongChars = [];
for (const line of input) {
    wrongChars.push(...findWrongCharacter(line));
}

function findWrongCharacter(line) {
    const stack = [];

    for (const character of line) {
        if (stack.length != 0) {
            if(openingSegments.has(character)) {
                stack.push(character);
                continue;
            }

            if (closingSegments.has(character)) {
                let top = peek(stack);
                
                if(character == pairs[top]) {
                    stack.pop();
                } else {
                    return [ character ]
                }
            }
        } else {
            if(openingSegments.has(character)) {
                stack.push(character);
            }
        }
    }
    return [];
}

console.log(wrongChars.map(v=>scoringTable[v]).reduce((agg,cur)=>agg+cur,0));