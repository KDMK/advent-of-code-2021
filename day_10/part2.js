const fs = require("fs");

const openingSegments = new Set(['(', '{', '[', '<']);
const closingSegments = new Set([")", "}", "]", ">"]);

const pairs = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">",
}

const peek = (stack) => stack[stack.length - 1]

let scoringTable = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(line => line.split(""));

function findCompletionBrackets(line) {
    const stack = [];

    for (const character of line) {
        if (stack.length != 0) {
            if(openingSegments.has(character)) {
                stack.push(character);
                continue;
            }

            if (closingSegments.has(character)) {
                let top = peek(stack);
                
                if(character == pairs[top]) {
                    stack.pop();
                } else {
                    return []
                }
            }
        } else {
            if(openingSegments.has(character)) {
                stack.push(character);
            }
        }
    }

    return stack.map(v=>pairs[v]).reverse();
}

function calculateScore(array) {
    let score = 0;
    for (let element of array) {
        score *= 5;
        score += scoringTable[element];
    }

    return score;
}

const scores = [];
for (const line of input) {
    let completionLine = findCompletionBrackets(line);
    if (completionLine.length == 0) {
        continue
    }
    scores.push(calculateScore(completionLine));
}

scores.sort((a,b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    else return 0;
});
console.log(scores[Math.floor(scores.length/2)]);