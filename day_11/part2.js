const fs = require("fs");

let totalFlashes = 0;

function printMatrix(matrix) {
    for (let row of matrix) {
        console.log(row.join(""))
    }
}

function createMatrix(rows, cols) {
    const retVal = [];
    for (let i = 0; i < rows; i++) {
        retVal.push(new Array(cols).fill(false));
    }

    return retVal;
}

function flash(x, y, array, visited, flashMatrix) {
    if (typeof array[x] == 'undefined') {
        return;
    }

    if (typeof array[x][y] == 'undefined') {
        return;
    }

    if (visited[x][y]) {
        return
    }

    visited[x][y] = true;

    if (array[x][y] == 0 && flashMatrix[x][y]) {
        return;
    }

    array[x][y]++;

    if (array[x][y] <= 9) {
        return;
    }

    array[x][y] = 0;
    flashMatrix[x][y] = true;

    let visitorMatrix1 = createMatrix(10, 10);
    flash(x - 1, y - 1, array, visitorMatrix1, flashMatrix);

    let visitorMatrix2 = createMatrix(10, 10);
    flash(x, y - 1, array, visitorMatrix2, flashMatrix);

    let visitorMatrix3 = createMatrix(10, 10);
    flash(x + 1, y - 1, array, visitorMatrix3, flashMatrix);

    let visitorMatrix4 = createMatrix(10, 10);
    flash(x - 1, y, array, visitorMatrix4, flashMatrix);

    let visitorMatrix5 = createMatrix(10, 10);
    flash(x + 1, y, array, visitorMatrix5, flashMatrix);

    let visitorMatrix6 = createMatrix(10, 10);
    flash(x - 1, y + 1, array, visitorMatrix6, flashMatrix);

    let visitorMatrix7 = createMatrix(10, 10);
    flash(x, y + 1, array, visitorMatrix7, flashMatrix);

    let visitorMatrix8 = createMatrix(10, 10);
    flash(x + 1, y + 1, array, visitorMatrix8, flashMatrix);
}

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(line => line.split("").map(v => Number.parseInt(v)));

function dumboCycle() {
    let flashMatrix = createMatrix(10, 10);
    for (let i = 0; i < input.length; i++) {
        for (let j = 0; j < input.length; j++) {
            let visitorMatrix = createMatrix(10, 10);
            flash(i, j, input, visitorMatrix, flashMatrix);
        }
    }
}

let i = 1;
while (true) {
    dumboCycle();

    let val = input.map(v => v.reduce((agg, cur) => agg + cur, 0))
        .reduce((agg, cur) => agg + cur, 0);

    if (val == 0) {
        console.log(i);
        break;
    }

    i++;
}

