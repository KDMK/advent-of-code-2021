const fs = require("fs");

const args = process.argv.slice(2);

let maxX = 0;
let maxY = 0;

const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(v => {
        const a = v.split(" -> ");

        
        let p1 = a[0].split(",").map(v => {
            return Number.parseInt(v);
        });
        let p2 = a[1].split(",").map(v => {
            return Number.parseInt(v);
        });

        let points1 = {x: p1[0], y: p1[1]}
        let points2 = {x: p2[0], y: p2[1]}

        if(points1.x > maxX) {
            maxX = points1.x;
        }

        if(points2.x > maxX) {
            maxX = points2.x;
        }

        if(points1.y > maxY) {
            maxY = points1.y;
        }

        if(points2.y > maxY) {
            maxY = points2.y;
        }

        return [points1, points2]
    })
    .filter(v => {
        return v[0].x == v[1].x || v[0].y == v[1].y;
    });

const array = [];
for(let j = 0; j <= maxY; j++) {
    array.push(new Array(maxX+1).fill(0));
}

for(let pair of input) {
    if(pair[0].x == pair[1].x) {
        // vertical line
        let start = pair[0].y > pair[1].y ? pair[1].y : pair[0].y;
        let end = pair[0].y > pair[1].y ? pair[0].y : pair[1].y;

        for(let pointer = start; pointer <= end; pointer++) {
            array[pointer][pair[0].x] += 1;
        }
    } else {
        // horizontal line
        let start = pair[0].x > pair[1].x ? pair[1].x : pair[0].x;
        let end = pair[0].x > pair[1].x ? pair[0].x : pair[1].x;

        for(let pointer = start; pointer <= end; pointer++) {
            array[pair[0].y][pointer] += 1;
        }
    }
}

let out = "";
let over2 = 0;
for(let i = 0; i < array.length; i++) {
    for(let j = 0; j < array[0].length; j++) {
        out += `${array[i][j]} `;
        if (array[i][j] > 1) {
            over2++;
        }
    }
    out += '\n';
}

console.log(over2);
