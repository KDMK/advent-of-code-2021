const fs = require("fs");

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .flatMap(v => {
        if (v.trim() === '') {
            return [];
        }

        const parts = v.split(" ");
        return [{ direction: parts[0], amount: Number.parseInt(parts[1]) }];
    });

function runSubmarine(inputSequence, startingPoint) {
    const endPoint = {
        x: startingPoint.x,
        y: startingPoint.y
    };

    let aim = 0;
    for (const entry of inputSequence) {
        if (entry.direction === 'forward') {
            endPoint.x += entry.amount;
            endPoint.y += (aim * entry.amount);
        } else if (entry.direction === 'down') {
            aim += entry.amount;
        } else if (entry.direction === 'up') {
            aim -= entry.amount;
        } else {
            throw "Not supported: " + entry.direction;
        }
    }

    return endPoint;
}

const startingPoint = {x: 0, y: 0};
const endPoint = runSubmarine(input, startingPoint);

console.log(endPoint.x * endPoint.y);