const fs = require("fs");

const segmentsMap = {
    "ab": "1",
    "acdfg": "2",
    "abcdf": "3",
    "abef": "4",
    "bcdef": "5",
    "bcdefg": "6",
    "abd": "7",
    "abcdefg": "8",
    "abcdef": "9",
    "abcdeg": "0",
}

const segmentsMapReverse = {
    "1": "cf",
    "2": "acdeg",
    "3": "acdfg",
    "4": "bcdf",
    "5": "abdfg",
    "6": "abdefg",
    "7": "acf",
    "8": "abcdefg",
    "9": "abcdfg",
    "0": "abcefg",
}

function removeItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
  }

const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(line => {
        let splitted = line.split(" | ");
        let dict = splitted[0].split(" ").map(v => v.split("").sort().join(""));
        let code = splitted[1].split(" ").map(v => v.split("").sort().join(""));

        return { dict, code }
    });



// -> from one we can always interpret which segments are two left ones
// -> then from 7 we can determine top one
// 4 + 7 = 9
// 

function createDict(combinations) {
    const dict = {}

    let rest = { "6": [], "5": [] };
    // find easy ones
    for (let combination of combinations) {
        switch (combination.length) {
            case 2: // one
                dict["1"] = combination;
                break;
            case 3: // seven
                dict["7"] = combination;
                break;
            case 4: // four
                dict["4"] = combination;
                break;
            case 7: // eight
                dict["8"] = combination;
                break;
            default:
                let bla = "" + combination.length;
                rest[bla].push(combination);
        }
    }

    let a = dict["4"].split("")
    let b = dict["7"].split("")
    let set9 = new Set([...a, ...b]);

    let middleSegmentSearched = dict["4"].split("").filter(v => dict["1"].split("").indexOf(v) == -1);

    // process 6
    while (rest["6"].length !== 0) {
        if (typeof dict["9"] == 'undefined') {
            let nine;
            for (let combination of rest["6"]) {
                let letters1 = combination.split("");

                let fu = letters1.filter(v => !set9.has(v));

                if (fu.length == 1) {
                    nine = combination;
                    break;
                }
            }
            if (typeof nine != 'undefined') {
                dict["9"] = nine;
                removeItemOnce(rest["6"], nine);
            }
        } else {
            for (let combination of rest["6"]) {
                let pos = middleSegmentSearched.filter(v => combination.split("").indexOf(v) === -1);
                if (pos.length === 0) {
                    dict["6"] = combination;
                    rest["6"].shift();
                } else {
                    dict["0"] = combination;
                    rest["6"].shift();
                }
            }
        }
    }

    // process 5
    while (rest["5"].length !== 0) {
        if (typeof dict["3"] == 'undefined') {
            let three;
            for (let combination of rest["5"]) {
                let set2 = combination.split("").filter(v => dict["1"].split("").indexOf(v) == -1);

                if (set2.length == 3) {
                    three = combination;
                    break;
                }
            }
            if (typeof three != 'undefined') {
                dict["3"] = three;
                removeItemOnce(rest["5"], three);
            }
        } else {
            // zbroji combination + 1 i vidi jel 9; 2 nece dati nikakav validan broj
            let broj = Array.from(new Set([...rest["5"][0].split(""), ...dict["1"].split("")])).sort().join("");

            if (dict["9"] == broj) {
                dict["5"] = rest["5"][0];
                rest["5"].shift();
            } else {
                dict["2"] = rest["5"][0];
                rest["5"].shift();
            }
        }

    }

    let reverseDict = {};

    Object.keys(dict).forEach(v => {
        reverseDict[dict[v]] = v;
    })

    return reverseDict;
}

let results = []

for (let inputCombination of input) {
    let dict = createDict(inputCombination.dict);
    let number = Number.parseInt(inputCombination.code.map(v => dict[v]).join(""));

    results.push(number);
}


// let dict = createDict(input[7].dict);
// let number = Number.parseInt(input[7].code.map(v => dict[v]).join(""));

// results.push(number);


console.log(results.reduce((agg,cur)=>agg+cur,0));