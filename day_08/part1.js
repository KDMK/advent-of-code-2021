const fs = require("fs");

const segmentsMap = {
    "ab": "1",
    "gcdfa": "2",
    "fbcad": "3",
    "eafb": "4",
    "cdfbe": "5",
    "cdfgeb": "6",
    "dab": "7",
    "acedgfb": "8",
    "cefabd": "9",
    "cagedb": "0",
}

const segmentsMapReverse = {
    "1" : "cf",
    "2" : "acdeg",
    "3" : "acdfg",
    "4" : "bcdf",
    "5" : "abdfg",
    "6" : "abdefg",
    "7" : "acf",
    "8" : "abcdefg",
    "9" : "abcdfg",
    "0" : "abcefg",
}


const args = process.argv.slice(2);
const input = fs.readFileSync(`./${args[0]}`, "utf-8")
    .split("\n")
    .map(line => {
        let splitted = line.split(" | ");
        return splitted[1].split(" ")
                          .map(v => v.split("").sort().join(""))
    });

const occurrences = {
    "1": 0,
    "4": 0,
    "7": 0,
    "8": 0,
    
}

for(let instance of input) {
    for(let digit of instance) {
        switch(digit.length) {
            case 2: // one
                occurrences["1"]++;
                break;
            case 3: // seven
                occurrences["7"]++;
                break;
            case 4: // four
                occurrences["4"]++;
                break;
            case 7: // eight
                occurrences["8"]++;
                break;
        }
    }
}

let sum = occurrences["1"] +
          occurrences["4"] +
          occurrences["7"] +
          occurrences["8"];

console.log(sum);